package ru.rnt.rickandmorty.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@RequiredArgsConstructor
@RestController
public class RickAndMortyCharactersController {

    private static final String RICK_AND_MORTY_API_CHARACTERS_URL =
            "https://rickandmortyapi.com/api/character";
    private static final String CHARACTERS_URL_PART = "/characters?page=";
    private static final String PAGE_URL_PART = "?page=";

    @GetMapping("/characters")
    public String findAllByPage(@RequestParam Integer page) {
        return WebClient.create().get()
                .uri(RICK_AND_MORTY_API_CHARACTERS_URL,
                        uri -> uri.queryParam("page", page).build())
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(String.class)
                .map(response -> response.replace(
                        RICK_AND_MORTY_API_CHARACTERS_URL + PAGE_URL_PART,
                        CHARACTERS_URL_PART
                )).block();
    }
}
