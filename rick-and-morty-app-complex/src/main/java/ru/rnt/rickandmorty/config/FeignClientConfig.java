package ru.rnt.rickandmorty.config;

import feign.ResponseInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rnt.rickandmorty.repositories.RickAndMortyResponseInterceptor;

@Configuration
public class FeignClientConfig {
    @Bean
    public ResponseInterceptor responseInterceptor(){
        return new RickAndMortyResponseInterceptor();
    }
}
