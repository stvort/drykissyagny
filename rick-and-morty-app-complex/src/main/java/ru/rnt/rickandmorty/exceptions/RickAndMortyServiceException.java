package ru.rnt.rickandmorty.exceptions;

public class RickAndMortyServiceException extends RuntimeException {
    public RickAndMortyServiceException(String message) {
        super(message);
    }

    public RickAndMortyServiceException(Throwable cause) {
        super(cause);
    }

}
