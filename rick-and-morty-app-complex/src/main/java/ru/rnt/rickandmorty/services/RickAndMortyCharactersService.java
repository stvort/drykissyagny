package ru.rnt.rickandmorty.services;

import ru.rnt.rickandmorty.projections.RickAndMortyCharactersPage;

public interface RickAndMortyCharactersService {
    RickAndMortyCharactersPage findAllByPage(int page);
}
