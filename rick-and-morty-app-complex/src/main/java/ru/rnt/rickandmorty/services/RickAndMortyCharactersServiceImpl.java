package ru.rnt.rickandmorty.services;

import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rnt.rickandmorty.projections.RickAndMortyCharactersPage;
import ru.rnt.rickandmorty.exceptions.RickAndMortyServiceException;
import ru.rnt.rickandmorty.repositories.RickAndMortyCharactersRepository;

@Slf4j
@RequiredArgsConstructor
@Service
public class RickAndMortyCharactersServiceImpl implements RickAndMortyCharactersService {

    private final RickAndMortyCharactersRepository charactersRepository;

    @Retry(name = "rickAndMortyApi")
    @Override
    public RickAndMortyCharactersPage findAllByPage(int page) {
        try {
            return charactersRepository.findAllByPage(page);
        } catch (Exception e) {
            throw new RickAndMortyServiceException(e);
        }
    }
}
