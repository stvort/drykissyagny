package ru.rnt.rickandmorty.repositories;

import feign.InvocationContext;
import feign.ResponseInterceptor;
import ru.rnt.rickandmorty.projections.RickAndMortyCharactersPage;

import static java.util.Objects.nonNull;

public class RickAndMortyResponseInterceptor implements ResponseInterceptor {

    @Override
    public Object aroundDecode(InvocationContext invocationContext) {
        var rawResponse = invocationContext.proceed();
        if (rawResponse instanceof RickAndMortyCharactersPage response) {
            return replacePageInfo(response);
        }

        return rawResponse;
    }

    private RickAndMortyCharactersPage replacePageInfo(RickAndMortyCharactersPage response) {
        var newNext = replaceExternalUrl(response.getInfo().getNext());
        var newPrev = replaceExternalUrl(response.getInfo().getPrev());
        response.getInfo().setNext(newNext);
        response.getInfo().setPrev(newPrev);
        return response;
    }

    private String replaceExternalUrl(String url) {
        if (nonNull(url)) {
            url = url.replaceFirst( "^.*\\?", "/characters?");
        }
        return url;
    }
}
