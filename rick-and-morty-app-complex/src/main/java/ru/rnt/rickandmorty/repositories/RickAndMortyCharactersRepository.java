package ru.rnt.rickandmorty.repositories;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.rnt.rickandmorty.config.FeignClientConfig;
import ru.rnt.rickandmorty.projections.RickAndMortyCharactersPage;

@FeignClient(name = "rick-and-morty-client", url = "${rick.and.morty.api-base-url}",
        configuration = FeignClientConfig.class)
public interface RickAndMortyCharactersRepository {

    @GetMapping("/character")
    RickAndMortyCharactersPage findAllByPage(@RequestParam int page);
}
