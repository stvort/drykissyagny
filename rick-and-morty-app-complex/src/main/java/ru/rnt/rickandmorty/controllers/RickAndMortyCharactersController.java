package ru.rnt.rickandmorty.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.rnt.rickandmorty.projections.RickAndMortyCharactersPage;
import ru.rnt.rickandmorty.services.RickAndMortyCharactersService;

@RequiredArgsConstructor
@RestController
public class RickAndMortyCharactersController {

    private final RickAndMortyCharactersService charactersService;

    @Cacheable(value = "characters")
    @GetMapping("/characters")
    public RickAndMortyCharactersPage findAllByPage(@RequestParam(defaultValue = "1") Integer page)  {
        return charactersService.findAllByPage(page);
    }
}
